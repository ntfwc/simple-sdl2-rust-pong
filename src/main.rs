mod sdl;
use sdl::sdl_font::*;
use sdl::sdl_renderer::SdlRenderer;
use sdl::sdl_session::SdlSession;
use sdl::sdl_window::SdlWindow;
use sdl::*;

mod game_objects;
use game_objects::dimension::Dimension;

use game_objects::input_manager::*;

use game_objects::ball::Ball;
use game_objects::paddle;
use game_objects::paddle::*;
use game_objects::score::Score;

use std::cell::Cell;
use std::ffi::CString;
use std::rc::Rc;

const BACKGROUND_COLOR: SdlColor = SdlColor {
    r: 0,
    g: 0,
    b: 0,
    a: 0,
};

const DIVIDER_COLOR: SdlColor = SdlColor {
    r: 255,
    g: 255,
    b: 255,
    a: 255,
};

const SCREEN_DIMENSIONS: Dimension = Dimension {
    width: 480u32,
    height: 360u32,
};

const CENTER_X: i32 = (SCREEN_DIMENSIONS.width / 2) as i32;

const FONT_SIZE: u32 = 16;
const FONT_PATH: &str = "res/ttf-bitstream-vera-1.10/VeraMono.ttf";

const TITLE: &str = "Pong";

struct Main {
    input_manager: InputManager,
    paddle1: Paddle,
    paddle2: Paddle,
    ball: Ball,
    font: SdlFont,
    player1_score: Score,
    player2_score: Score,
}

fn main_loop(main: &mut Main, renderer: &mut SdlRenderer) -> bool {
    let mut keep_running: bool = true;
    renderer.poll_events(&mut |event| match event {
        Event::QUIT => keep_running = false,
        Event::UNKNOWN => {}
        Event::KEYDOWN(key) => main.input_manager.handle_key_down(key),
        Event::KEYUP(key) => main.input_manager.handle_key_up(key),
    });
    if main.input_manager.is_key_pressed(InputKey::Player1Down) {
        main.paddle1.move_down(&SCREEN_DIMENSIONS);
    }
    if main.input_manager.is_key_pressed(InputKey::Player1Up) {
        main.paddle1.move_up(&SCREEN_DIMENSIONS);
    }
    if main.input_manager.is_key_pressed(InputKey::Player2Down) {
        main.paddle2.move_down(&SCREEN_DIMENSIONS);
    }
    if main.input_manager.is_key_pressed(InputKey::Player2Up) {
        main.paddle2.move_up(&SCREEN_DIMENSIONS);
    }
    main.ball
        .apply_movement(&SCREEN_DIMENSIONS, &main.paddle1, &main.paddle2);

    renderer.clear(&BACKGROUND_COLOR);

    main.paddle1.draw(renderer);
    main.paddle2.draw(renderer);
    main.ball.draw(renderer);
    main.player1_score.draw(renderer, &main.font);
    main.player2_score.draw(renderer, &main.font);

    for y in (0..(SCREEN_DIMENSIONS.height as i32)).step_by(4) {
        renderer.draw_point(&DIVIDER_COLOR, CENTER_X, y);
    }

    keep_running
}

fn run(session: &SdlSession, renderer: &mut SdlRenderer) {
    let input_manager = InputManager::new();

    let paddle1 = Paddle::new(20, &SCREEN_DIMENSIONS);
    let paddle2 = Paddle::new(
        (SCREEN_DIMENSIONS.width - (20 + paddle::SIZE.width)) as i32,
        &SCREEN_DIMENSIONS,
    );

    let mut ball = {
        let ball_half_size: i32 = Ball::SIZE as i32 / 2;
        Ball::new(
            (SCREEN_DIMENSIONS.width as i32 - ball_half_size) as i32 / 2,
            (SCREEN_DIMENSIONS.height as i32 - ball_half_size) / 2,
        )
    };

    let font = {
        let font_path = CString::new(FONT_PATH).unwrap();
        let font_optional = SdlFont::new(session, font_path.as_c_str(), FONT_SIZE);
        match font_optional {
            Some(font) => font,
            None => panic!("Failed to open the font: {}", FONT_PATH),
        }
    };

    let player1_score = Score::new(SCREEN_DIMENSIONS.width as i32 / 2 - 100, 20);
    let player2_score = Score::new(SCREEN_DIMENSIONS.width as i32 / 2 + 100, 20);

    let player1_score_value = player1_score.value.clone();
    let increment_score = |score_value: &Rc<Cell<u16>>| {
        let mut new_value = score_value.get() + 1;
        const MAX_VALUE: u16 = 1000;
        if new_value > MAX_VALUE {
            new_value = MAX_VALUE
        }
        score_value.set(new_value);
    };
    ball.set_on_exit_right_listener(Box::new(move || increment_score(&player1_score_value)));
    let player2_score_value = player2_score.value.clone();
    ball.set_on_exit_left_listener(Box::new(move || increment_score(&player2_score_value)));

    let mut main = Main {
        input_manager,
        paddle1,
        paddle2,
        ball,
        font,
        player1_score,
        player2_score,
    };

    while main_loop(&mut main, renderer) {
        renderer.present();
        sdl_delay(1000 / 60);
    }
}

fn main() {
    let session: SdlSession = SdlSession::new();
    let title = CString::new(TITLE).unwrap();
    let window = SdlWindow::new(
        &session,
        title.as_c_str(),
        SCREEN_DIMENSIONS.width,
        SCREEN_DIMENSIONS.height,
    );
    let mut renderer = window.create_renderer();
    run(&session, &mut renderer);
}
