use std::env;
use std::path::PathBuf;

fn main() {
    println!(r"cargo:rustc-link-lib=dylib=sdl_bridge");
    if cfg!(feature = "mingw") {
        get_link_search_path_mingw();
    }
    else {
        get_link_search_path();
    }
}

#[cfg(all(not(target_os = "windows"), debug_assertions))]
fn get_link_search_path() {
    let current_dir: PathBuf = env::current_dir().unwrap();
    set_link_search_path(current_dir);
}

#[cfg(not(debug_assertions))]
fn get_link_search_path() {
    let mut current_dir: PathBuf = env::current_dir().unwrap();
    current_dir.push("release");
    set_link_search_path(current_dir);
}

fn get_link_search_path_mingw() {
    let mut current_dir: PathBuf = env::current_dir().unwrap();
    current_dir.push("release-mingw");
    set_link_search_path(current_dir);
}

fn set_link_search_path(path: PathBuf) {
    println!(
        r"cargo:rustc-link-search=native={}",
        path.into_os_string().into_string().unwrap()
    );
}
