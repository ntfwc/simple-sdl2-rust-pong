#include "SDL_pixels.h"
#include "SDL_render.h"
#include <SDL.h>
#include <SDL_ttf.h>
#include <stdbool.h>
#include <stdio.h>

bool bridge_SDL_Init()
{
	return SDL_Init(SDL_INIT_VIDEO) == 0 ? true : false;
}

void bridge_SDL_Quit()
{
	SDL_Quit();
}

SDL_Window* bridge_SDL_CreateWindow(char* title, uint32_t window_width, uint32_t window_height)
{
	return SDL_CreateWindow(title, SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, window_width, window_height, SDL_WINDOW_SHOWN);
}

void bridge_SDL_DestroyWindow(SDL_Window *window)
{
	SDL_DestroyWindow(window);
}

SDL_Renderer *bridge_SDL_CreateRenderer(SDL_Window *window)
{
	return SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
}

void bridge_SDL_DestroyRenderer(SDL_Renderer *renderer)
{
	SDL_DestroyRenderer(renderer);
}

void bridge_SDL_RenderPresent(SDL_Renderer *renderer)
{
	SDL_RenderPresent(renderer);
}

void bridge_SDL_Delay(uint32_t milliseconds)
{
	SDL_Delay(milliseconds);
}

enum bridge_event_type
{
	QUIT, UNKNOWN, KEYDOWN, KEYUP
};

enum bridge_key_type
{
	KEY_W, KEY_S, KEY_UP, KEY_DOWN, KEY_UNKNOWN
};

struct bridge_event
{
	enum bridge_event_type event_type;
	enum bridge_key_type key_type;
};

static enum bridge_key_type translate_key(int sdl_key_code)
{
	switch (sdl_key_code)
	{
		case SDLK_w:
			return KEY_W;
		case SDLK_s:
			return KEY_S;
		case SDLK_UP:
			return KEY_UP;
		case SDLK_DOWN:
			return KEY_DOWN;
		default:
			return KEY_UNKNOWN;
	}
}

struct bridge_event *bridge_poll_event()
{
	static SDL_Event e;
	static struct bridge_event event;

	if (SDL_PollEvent(&e) == 0)
		return NULL;

	switch (e.type)
	{
		case SDL_QUIT:
			event.event_type = QUIT;
			break;
		case SDL_KEYDOWN:
			event.event_type = KEYDOWN;
			event.key_type = translate_key(e.key.keysym.sym);
			break;
		case SDL_KEYUP:
			event.event_type = KEYUP;
			event.key_type = translate_key(e.key.keysym.sym);
			break;
		default:
			event.event_type = UNKNOWN;
	}

	return &event;
}

static void set_color(SDL_Renderer *renderer, const SDL_Color *color)
{
	SDL_SetRenderDrawColor(renderer, color->r, color->g, color->b, color->a);
}

void bridge_SDL_RenderClear(SDL_Renderer *renderer, const SDL_Color *color)
{
	set_color(renderer, color);
	SDL_RenderClear(renderer);
}

void bridge_SDL_RenderFillRect(SDL_Renderer *renderer, const SDL_Color *color, const SDL_Rect *rectangle)
{
	set_color(renderer, color);
	SDL_RenderFillRect(renderer, rectangle);
}

void bridge_SDL_RenderDrawPoint(SDL_Renderer *renderer, const SDL_Color *color, int32_t x, int32_t y)
{
	set_color(renderer, color);
	SDL_RenderDrawPoint(renderer, x, y);
}

bool bridge_SDL_ttf_init()
{
	return TTF_Init() == 0;
}

void bridge_SDL_ttf_quit()
{
	TTF_Quit();
}

TTF_Font *bridge_SDL_ttf_open_font(char *font_path, uint32_t size)
{
	return TTF_OpenFont(font_path, size);
}

void bridge_SDL_ttf_close_font(TTF_Font *font)
{
	TTF_CloseFont(font);
}

SDL_Surface *bridge_TTF_RenderText_Solid(TTF_Font *font, char *text, const SDL_Color color)
{
	return TTF_RenderText_Solid(font, text, color);
}

void bridge_SDL_FreeSurface(SDL_Surface *surface)
{
	SDL_FreeSurface(surface);
}

SDL_Texture *bridge_SDL_CreateTextureFromSurface(SDL_Renderer *renderer, SDL_Surface *surface)
{
	return SDL_CreateTextureFromSurface(renderer, surface);
}

void bridge_SDL_DestroyTexture(SDL_Texture *texture)
{
	SDL_DestroyTexture(texture);
}

void bridge_SDL_RenderCopy(SDL_Renderer *renderer, SDL_Texture *texture, SDL_Rect *source_rect, SDL_Rect *destination_rect)
{
	SDL_RenderCopy(renderer, texture, source_rect, destination_rect);
}

struct TextureQueryResult {
	bool success;
	uint32_t width;
        uint32_t height;
};

struct TextureQueryResult bridge_SDL_QueryTexture(SDL_Texture *texture)
{
	int width, height;
	if (SDL_QueryTexture(texture, NULL, NULL, &width, &height) != 0)
	{
	        struct TextureQueryResult result = { false, 0, 0 };
		return result;
	}
	struct TextureQueryResult result = { true, width, height };
	return result;
}

bool bridge_SDL_Intersect_Rect(const SDL_Rect *A, const SDL_Rect *B, SDL_Rect *result)
{
	return SDL_IntersectRect(A, B, result);
}
