use super::sdl_bridge::*;
pub struct SdlSession;

impl SdlSession {
    pub fn new() -> SdlSession {
        let init =
            |name: &'static str, init_method: &dyn Fn() -> bool, pre_fail_method: &dyn Fn()| {
                println!("Initializing {}", name);
                if !init_method() {
                    pre_fail_method();
                    panic!("Failed to initialize {}", name);
                }
                println!("Initialized {}", name);
            };
        init("SDL2", &sdl_init, &|| {});
        init("SDL TTF", &sdl_ttf_init, &|| quit_sdl2());
        SdlSession
    }
}

fn quit(name: &'static str, quit_method: &dyn Fn()) {
    println!("Quitting {}", name);
    quit_method();
    println!("Quit {}", name);
}

fn quit_sdl2() {
    quit("SDL2", &sdl_quit);
}

impl Drop for SdlSession {
    fn drop(&mut self) {
        quit("SDL TTF", &sdl_ttf_quit);
        quit_sdl2();
    }
}
