use crate::Dimension;
use std::ptr::NonNull;

#[repr(C)]
pub struct CSdlWindow {
    private: [u8; 0],
}

#[repr(C)]
pub struct CSdlRenderer {
    private: [u8; 0],
}

#[repr(C)]
pub struct CSdlFont {
    private: [u8; 0],
}

#[repr(C)]
pub struct CSdlSurface {
    private: [u8; 0],
}

#[repr(C)]
pub struct CSdlTexture {
    private: [u8; 0],
}

#[repr(C)]
#[allow(dead_code)]
#[derive(Copy, Clone)]
pub enum EventType {
    QUIT,
    UNKNOWN,
    KEYDOWN,
    KEYUP,
}

#[repr(C)]
#[allow(dead_code, non_camel_case_types)]
#[derive(Copy, Clone, Debug)]
pub enum KeyType {
    KEY_W,
    KEY_S,
    KEY_UP,
    KEY_DOWN,
    KEY_UNKNOWN,
}

#[repr(C)]
struct CEvent {
    event_type: EventType,
    key_type: KeyType,
}

pub enum Event {
    QUIT,
    UNKNOWN,
    KEYDOWN(KeyType),
    KEYUP(KeyType),
}

#[repr(C)]
#[derive(Debug, Default)]
pub struct SdlRect {
    pub x: i32,
    pub y: i32,
    pub w: i32,
    pub h: i32,
}

#[repr(C)]
pub struct SdlColor {
    pub r: u8,
    pub g: u8,
    pub b: u8,
    pub a: u8,
}

#[repr(C)]
struct CTextureQueryResult {
    success: bool,
    width: u32,
    height: u32,
}

extern "C" {
    fn bridge_SDL_Init() -> bool;
    fn bridge_SDL_Quit();
    fn bridge_SDL_CreateWindow(
        title: *const u8,
        window_width: u32,
        window_height: u32,
    ) -> *mut CSdlWindow;
    fn bridge_SDL_DestroyWindow(window: *mut CSdlWindow);
    fn bridge_SDL_CreateRenderer(window: *mut CSdlWindow) -> *mut CSdlRenderer;
    fn bridge_SDL_DestroyRenderer(window: *mut CSdlRenderer);
    fn bridge_SDL_Delay(milliseconds: u32);
    fn bridge_SDL_RenderPresent(renderer: *mut CSdlRenderer);
    fn bridge_poll_event() -> *mut CEvent;
    fn bridge_SDL_RenderClear(renderer: *mut CSdlRenderer, color: *const SdlColor);
    fn bridge_SDL_RenderFillRect(
        renderer: *mut CSdlRenderer,
        color: *const SdlColor,
        rectangle: *const SdlRect,
    );
    fn bridge_SDL_RenderDrawPoint(
        renderer: *mut CSdlRenderer,
        color: *const SdlColor,
        x: i32,
        y: i32,
    );
    fn bridge_SDL_ttf_init() -> bool;
    fn bridge_SDL_ttf_quit();
    fn bridge_SDL_ttf_open_font(font_path: *const u8, size: u32) -> *mut CSdlFont;
    fn bridge_SDL_ttf_close_font(font: *mut CSdlFont);
    fn bridge_TTF_RenderText_Solid(
        font: *const CSdlFont,
        text: *const u8,
        color: SdlColor,
    ) -> *mut CSdlSurface;
    fn bridge_SDL_FreeSurface(surface: *mut CSdlSurface);
    fn bridge_SDL_CreateTextureFromSurface(
        renderer: *mut CSdlRenderer,
        surface: *mut CSdlSurface,
    ) -> *mut CSdlTexture;
    fn bridge_SDL_DestroyTexture(texture: *mut CSdlTexture);
    fn bridge_SDL_RenderCopy(
        renderer: *mut CSdlRenderer,
        surface: *const CSdlTexture,
        source_rect: *const SdlRect,
        destination_rect: *const SdlRect,
    );
    fn bridge_SDL_QueryTexture(texture: *mut CSdlTexture) -> CTextureQueryResult;
    fn bridge_SDL_Intersect_Rect(
        rect1: *const SdlRect,
        rect2: *const SdlRect,
        result: *mut SdlRect,
    ) -> bool;
}

pub fn sdl_init() -> bool {
    unsafe { bridge_SDL_Init() }
}

pub fn sdl_quit() {
    unsafe {
        bridge_SDL_Quit();
    }
}

pub fn sdl_create_window(
    title: &[u8],
    window_width: u32,
    window_height: u32,
) -> Option<NonNull<CSdlWindow>> {
    validate_c_string(title);
    unsafe {
        NonNull::new(bridge_SDL_CreateWindow(
            title.as_ptr(),
            window_width,
            window_height,
        ))
    }
}

pub fn sdl_destroy_window(window: NonNull<CSdlWindow>) {
    unsafe {
        bridge_SDL_DestroyWindow(window.as_ptr());
    }
}

fn validate_c_string(string: &[u8]) {
    match string.last() {
        Some(x) => {
            if *x != 0u8 {
                panic!("Given invalid C string, it does not end with a null byte")
            }
        }
        _ => panic!("Given invalid C string, it is empty and does not end with a null byte"),
    }
}

pub fn sdl_create_renderer(window: NonNull<CSdlWindow>) -> Option<NonNull<CSdlRenderer>> {
    unsafe { NonNull::new(bridge_SDL_CreateRenderer(window.as_ptr())) }
}

pub fn sdl_destroy_renderer(renderer: NonNull<CSdlRenderer>) {
    unsafe {
        bridge_SDL_DestroyRenderer(renderer.as_ptr());
    }
}

pub fn sdl_delay(milliseconds: u32) {
    unsafe {
        bridge_SDL_Delay(milliseconds);
    }
}

pub fn sdl_present(renderer: NonNull<CSdlRenderer>) {
    unsafe {
        bridge_SDL_RenderPresent(renderer.as_ptr());
    }
}

pub fn sdl_poll_event() -> Option<Event> {
    unsafe {
        NonNull::new(bridge_poll_event()).and_then(|event| {
            let event_ref = event.as_ref();
            Some(match event_ref.event_type {
                EventType::QUIT => Event::QUIT,
                EventType::UNKNOWN => Event::UNKNOWN,
                EventType::KEYDOWN => Event::KEYDOWN(event_ref.key_type),
                EventType::KEYUP => Event::KEYUP(event_ref.key_type),
            })
        })
    }
}

pub fn sdl_clear(renderer: NonNull<CSdlRenderer>, color: &SdlColor) {
    unsafe {
        bridge_SDL_RenderClear(renderer.as_ptr(), color);
    }
}

pub fn sdl_draw_rect(renderer: NonNull<CSdlRenderer>, color: &SdlColor, rectangle: &SdlRect) {
    unsafe {
        bridge_SDL_RenderFillRect(renderer.as_ptr(), color, rectangle);
    }
}

pub fn sdl_draw_point(renderer: NonNull<CSdlRenderer>, color: &SdlColor, x: i32, y: i32) {
    unsafe {
        bridge_SDL_RenderDrawPoint(renderer.as_ptr(), color, x, y);
    }
}

pub fn sdl_ttf_init() -> bool {
    unsafe { bridge_SDL_ttf_init() }
}

pub fn sdl_ttf_quit() {
    unsafe { bridge_SDL_ttf_quit() }
}

pub fn sdl_ttf_open_font(file_path: &[u8], size: u32) -> Option<NonNull<CSdlFont>> {
    validate_c_string(file_path);
    unsafe { NonNull::new(bridge_SDL_ttf_open_font(file_path.as_ptr(), size)) }
}

pub fn sdl_ttf_close_font(font: NonNull<CSdlFont>) {
    unsafe { bridge_SDL_ttf_close_font(font.as_ptr()) }
}

pub fn sdl_ttf_render_solid_to_surface(
    font: NonNull<CSdlFont>,
    text: &[u8],
    color: SdlColor,
) -> Option<NonNull<CSdlSurface>> {
    validate_c_string(text);
    unsafe {
        NonNull::new(bridge_TTF_RenderText_Solid(
            font.as_ptr(),
            text.as_ptr(),
            color,
        ))
    }
}

pub fn sdl_free_surface(surface: NonNull<CSdlSurface>) {
    unsafe { bridge_SDL_FreeSurface(surface.as_ptr()) }
}

pub fn sdl_create_texture_from_surface(
    renderer: NonNull<CSdlRenderer>,
    surface: NonNull<CSdlSurface>,
) -> Option<NonNull<CSdlTexture>> {
    unsafe {
        NonNull::new(bridge_SDL_CreateTextureFromSurface(
            renderer.as_ptr(),
            surface.as_ptr(),
        ))
    }
}

pub fn sdl_destroy_texture(texture: NonNull<CSdlTexture>) {
    unsafe { bridge_SDL_DestroyTexture(texture.as_ptr()) }
}

pub fn sdl_query_texture_dimensions(texture: NonNull<CSdlTexture>) -> Option<Dimension> {
    unsafe {
        let query_result = bridge_SDL_QueryTexture(texture.as_ptr());
        if query_result.success {
            Some(Dimension {
                width: query_result.width,
                height: query_result.height,
            })
        } else {
            None
        }
    }
}

pub fn sdl_draw_texture(
    renderer: NonNull<CSdlRenderer>,
    texture: NonNull<CSdlTexture>,
    destination_rect: &SdlRect,
) {
    unsafe {
        bridge_SDL_RenderCopy(
            renderer.as_ptr(),
            texture.as_ptr(),
            std::ptr::null(),
            destination_rect,
        )
    }
}

pub fn sdl_get_intersection(rect1: &SdlRect, rect2: &SdlRect) -> Option<SdlRect> {
    unsafe {
        let mut result: SdlRect = Default::default();
        if !bridge_SDL_Intersect_Rect(rect1, rect2, &mut result) {
            return Option::None;
        }

        Option::Some(result)
    }
}
