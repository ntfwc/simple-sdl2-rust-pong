use super::sdl_bridge::*;
use super::sdl_session::SdlSession;
use super::sdl_surface::SdlSurface;
use std::ffi::CStr;
use std::ptr::NonNull;

pub struct SdlFont {
    font: NonNull<CSdlFont>,
}

impl SdlFont {
    pub fn new(_session: &SdlSession, file_path: &CStr, size: u32) -> Option<SdlFont> {
        sdl_ttf_open_font(file_path.to_bytes_with_nul(), size)
            .map(|c_font| SdlFont { font: c_font })
    }

    pub fn render_to_surface(&self, text: &CStr, color: SdlColor) -> Option<SdlSurface> {
        sdl_ttf_render_solid_to_surface(self.font, text.to_bytes_with_nul(), color)
            .map(|c_surface| SdlSurface { surface: c_surface })
    }
}

impl Drop for SdlFont {
    fn drop(&mut self) {
        sdl_ttf_close_font(self.font);
    }
}
