use super::sdl_bridge::*;
use super::sdl_renderer::SdlRenderer;
use super::sdl_session::SdlSession;
use std::ffi::CStr;
use std::marker::PhantomData;
use std::ptr::NonNull;

pub struct SdlWindow<'a> {
    c_window: NonNull<CSdlWindow>,
    title: PhantomData<&'a CStr>,
}

impl SdlWindow<'_> {
    pub fn new<'a>(
        _session: &SdlSession,
        title: &'a CStr,
        width: u32,
        height: u32,
    ) -> SdlWindow<'a> {
        println!("Opening SDL2 Window");
        let c_window = sdl_create_window(title.to_bytes_with_nul(), width, height)
            .expect("Failed to open an SDL2 window");
        SdlWindow {
            c_window,
            title: PhantomData,
        }
    }

    pub fn create_renderer(&self) -> SdlRenderer {
        println!("Creating SDL2 Renderer");
        let renderer =
            sdl_create_renderer(self.c_window).expect("Failed to create SDL2 renderer for window");
        SdlRenderer { renderer }
    }
}

impl Drop for SdlWindow<'_> {
    fn drop(&mut self) {
        println!("Closing SDL2 window");
        sdl_destroy_window(self.c_window);
        println!("Closed SDL2 window");
    }
}
