use super::sdl_bridge::*;
use std::ptr::NonNull;

pub struct SdlRenderer {
    pub(super) renderer: NonNull<CSdlRenderer>,
}

impl SdlRenderer {
    pub fn present(&mut self) {
        sdl_present(self.renderer);
    }

    pub fn poll_events(&mut self, event_handler: &mut dyn FnMut(Event)) {
        while let Some(event) = sdl_poll_event() {
            event_handler(event);
        }
    }

    pub fn clear(&mut self, color: &SdlColor) {
        sdl_clear(self.renderer, color);
    }

    pub fn draw_rect(&mut self, color: &SdlColor, rectangle: &SdlRect) {
        sdl_draw_rect(self.renderer, color, rectangle);
    }

    pub fn draw_point(&mut self, color: &SdlColor, x: i32, y: i32) {
        sdl_draw_point(self.renderer, color, x, y);
    }
}

impl Drop for SdlRenderer {
    fn drop(&mut self) {
        println!("Destroying SDL2 renderer");
        sdl_destroy_renderer(self.renderer);
        println!("Destroyed SDL2 renderer");
    }
}
