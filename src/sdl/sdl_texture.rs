use super::sdl_bridge::*;
use super::sdl_renderer::SdlRenderer;
use crate::Dimension;
use std::ptr::NonNull;

pub struct SdlTexture {
    texture: NonNull<CSdlTexture>,
}

impl SdlTexture {
    pub(super) fn new(texture: NonNull<CSdlTexture>) -> SdlTexture {
        SdlTexture { texture }
    }

    pub fn draw(&self, renderer: &mut SdlRenderer, destination_rect: &SdlRect) {
        sdl_draw_texture(renderer.renderer, self.texture, destination_rect)
    }

    pub fn query_dimensions(&self) -> Option<Dimension> {
        sdl_query_texture_dimensions(self.texture)
    }
}

impl Drop for SdlTexture {
    fn drop(&mut self) {
        sdl_destroy_texture(self.texture);
    }
}
