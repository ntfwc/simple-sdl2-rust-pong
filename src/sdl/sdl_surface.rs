use super::sdl_bridge::*;
use super::sdl_renderer::SdlRenderer;
use super::sdl_texture::SdlTexture;
use std::ptr::NonNull;

pub struct SdlSurface {
    pub(super) surface: NonNull<CSdlSurface>,
}

impl SdlSurface {
    pub fn create_texture(&mut self, renderer: &mut SdlRenderer) -> Option<SdlTexture> {
        sdl_create_texture_from_surface(renderer.renderer, self.surface).map(SdlTexture::new)
    }
}

impl Drop for SdlSurface {
    fn drop(&mut self) {
        sdl_free_surface(self.surface);
    }
}
