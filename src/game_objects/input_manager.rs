use crate::sdl::KeyType;
use std::collections::HashMap;

#[derive(PartialEq)]
enum KeyState {
    UP,
    DOWN,
}

#[derive(PartialEq, Eq, Hash)]
pub enum InputKey {
    Player1Down,
    Player1Up,
    Player2Down,
    Player2Up,
}

pub struct InputManager {
    state_map: HashMap<InputKey, KeyState>,
}

impl InputManager {
    pub fn new() -> InputManager {
        InputManager {
            state_map: HashMap::new(),
        }
    }

    fn handle_key(&mut self, key_type: KeyType, key_state: KeyState) {
        if let Some(input_key) = map_key(key_type) {
            self.state_map.insert(input_key, key_state);
        }
    }

    pub fn handle_key_down(&mut self, key_type: KeyType) {
        self.handle_key(key_type, KeyState::DOWN);
    }

    pub fn handle_key_up(&mut self, key_type: KeyType) {
        self.handle_key(key_type, KeyState::UP);
    }

    pub fn is_key_pressed(&self, input_key: InputKey) -> bool {
        let val = self.state_map.get(&input_key).unwrap_or(&KeyState::UP);
        val == &KeyState::DOWN
    }
}

fn map_key(key_type: KeyType) -> Option<InputKey> {
    match key_type {
        KeyType::KEY_W => Some(InputKey::Player1Up),
        KeyType::KEY_S => Some(InputKey::Player1Down),
        KeyType::KEY_UP => Some(InputKey::Player2Up),
        KeyType::KEY_DOWN => Some(InputKey::Player2Down),
        _ => None,
    }
}
