use crate::sdl_font::SdlFont;
use crate::sdl_renderer::SdlRenderer;
use crate::SdlColor;
use crate::SdlRect;
use std::cell::Cell;
use std::ffi::CString;
use std::rc::Rc;

const COLOR: SdlColor = SdlColor {
    r: 255,
    g: 255,
    b: 255,
    a: 255,
};

pub struct Score {
    pub value: Rc<Cell<u16>>,
    x: i32,
    y: i32,
}

impl Score {
    pub fn new(x: i32, y: i32) -> Score {
        Score {
            value: Rc::new(Cell::new(0)),
            x,
            y,
        }
    }

    pub fn draw(&self, renderer: &mut SdlRenderer, font: &SdlFont) {
        let number_string: CString = CString::new(self.value.get().to_string()).unwrap();
        let opt_surface = font.render_to_surface(number_string.as_c_str(), COLOR);
        if opt_surface.is_none() {
            return;
        }

        let mut surface = opt_surface.unwrap();

        let opt_texture = surface.create_texture(renderer);
        if opt_texture.is_none() {
            return;
        }

        let texture = opt_texture.unwrap();

        let opt_dimensions = texture.query_dimensions();
        if opt_dimensions.is_none() {
            return;
        }

        let dimensions = opt_dimensions.unwrap();

        let dest_rect = SdlRect {
            x: self.x,
            y: self.y,
            w: dimensions.width as i32,
            h: dimensions.height as i32,
        };

        texture.draw(renderer, &dest_rect);
    }
}
