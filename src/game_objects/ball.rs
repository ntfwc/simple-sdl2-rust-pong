use super::dimension::Dimension;
use super::paddle::Paddle;
use super::vector::Vec2D;
use crate::sdl::sdl_get_intersection;
use crate::sdl::sdl_renderer::SdlRenderer;
use crate::sdl::SdlColor;
use crate::sdl::SdlRect;
use std::ops::FnMut;

const COLOR: SdlColor = SdlColor {
    r: 255,
    g: 255,
    b: 255,
    a: 255,
};

const INITIAL_VELOCITY: Vec2D = Vec2D { x: 3, y: 3 };
const MAX_X_SPEED: i32 = 7;

pub struct Ball {
    rectangle: SdlRect,
    velocity: Vec2D,
    initial_position: Vec2D,
    on_exit_left: Option<Box<dyn FnMut()>>,
    on_exit_right: Option<Box<dyn FnMut()>>,
}

fn handle_listener(listener: &mut Option<Box<dyn FnMut()>>) {
    if let Some(method) = listener {
        method();
    }
}

fn clamp_speed(value: i32, max: i32) -> i32 {
    if value > max {
        return max;
    } else if value < -max {
        return -max;
    }
    value
}

impl Ball {
    pub const SIZE: u32 = 10;

    pub fn new(initial_x: i32, initial_y: i32) -> Ball {
        Ball {
            rectangle: SdlRect {
                x: initial_x,
                y: initial_y,
                w: Ball::SIZE as i32,
                h: Ball::SIZE as i32,
            },
            velocity: INITIAL_VELOCITY,
            initial_position: Vec2D {
                x: initial_x,
                y: initial_y,
            },
            on_exit_left: Option::None,
            on_exit_right: Option::None,
        }
    }

    pub fn draw(&self, renderer: &mut SdlRenderer) {
        renderer.draw_rect(&COLOR, &self.rectangle);
    }

    fn reset_position(&mut self) {
        self.rectangle.x = self.initial_position.x;
        self.rectangle.y = self.initial_position.y;
    }

    pub fn apply_movement(
        &mut self,
        screen_dimension: &Dimension,
        paddle1: &Paddle,
        paddle2: &Paddle,
    ) {
        self.rectangle.x += self.velocity.x;
        self.rectangle.y += self.velocity.y;

        let reset = |ball: &mut Self| {
            ball.reset_position();
            let was_going_right = ball.velocity.x > 0;
            ball.velocity = INITIAL_VELOCITY;
            if was_going_right {
                ball.velocity.x = -ball.velocity.x;
            }
        };

        if self.rectangle.x < -(Self::SIZE as i32) {
            reset(self);
            handle_listener(&mut self.on_exit_left);
            return;
        }

        if self.rectangle.x > screen_dimension.width as i32 {
            reset(self);
            handle_listener(&mut self.on_exit_right);
            return;
        }

        let max_y: i32 = (screen_dimension.height as i32) - self.rectangle.h;
        let flip_y = |ball: &mut Self, new_y| {
            ball.rectangle.y = new_y;
            ball.velocity.y = -ball.velocity.y;
        };
        if self.rectangle.y < 0 {
            flip_y(self, -self.rectangle.y);
        } else if self.rectangle.y > max_y {
            flip_y(self, 2 * max_y - self.rectangle.y);
        }

        let find_intersection =
            |paddle: &Paddle| sdl_get_intersection(&self.rectangle, paddle.get_rectangle());
        if let Some(intersection) = find_intersection(&paddle1) {
            self.handle_paddle_collision(&intersection, true);
        } else if let Some(intersection) = find_intersection(&paddle2) {
            self.handle_paddle_collision(&intersection, false);
        }
    }

    pub fn set_on_exit_left_listener(&mut self, listener: Box<dyn FnMut()>) {
        self.on_exit_left = Option::Some(listener);
    }

    pub fn set_on_exit_right_listener(&mut self, listener: Box<dyn FnMut()>) {
        self.on_exit_right = Option::Some(listener);
    }

    fn handle_paddle_collision(&mut self, intersection: &SdlRect, is_left_paddle: bool) {
        let mut width = intersection.w;
        if !is_left_paddle {
            width = -width
        }
        self.rectangle.x += width * 2;

        let new_x_speed = -(self.velocity.x + (if self.velocity.x > 0 { 1 } else { -1 }));
        self.velocity.x = clamp_speed(new_x_speed, MAX_X_SPEED);
    }
}
