pub struct Dimension {
    pub width: u32,
    pub height: u32,
}
