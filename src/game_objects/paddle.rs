use super::dimension::Dimension;
use crate::sdl::sdl_renderer::*;
use crate::sdl::*;

const COLOR: SdlColor = SdlColor {
    r: 255,
    g: 255,
    b: 255,
    a: 255,
};

pub const SIZE: Dimension = Dimension {
    width: 10u32,
    height: 40u32,
};

const MOVE_SPEED: i32 = 4;

pub struct Paddle {
    rectangle: SdlRect,
}

impl Paddle {
    pub fn new(x: i32, screen_dimension: &Dimension) -> Paddle {
        Paddle {
            rectangle: SdlRect {
                x,
                y: ((screen_dimension.height / 2 - (SIZE.height / 2)) as i32),
                w: (SIZE.width as i32),
                h: (SIZE.height as i32),
            },
        }
    }
    pub fn draw(&self, renderer: &mut SdlRenderer) {
        renderer.draw_rect(&COLOR, &self.rectangle);
    }

    pub fn move_up(&mut self, _screen_dimension: &Dimension) {
        self.rectangle.y -= MOVE_SPEED;

        if self.rectangle.y < 0 {
            self.rectangle.y = 0
        }
    }

    pub fn move_down(&mut self, screen_dimension: &Dimension) {
        self.rectangle.y += MOVE_SPEED;

        let max_y: i32 = (screen_dimension.height as i32) - self.rectangle.h;
        if self.rectangle.y > max_y {
            self.rectangle.y = max_y;
        }
    }

    pub fn get_rectangle(&self) -> &SdlRect {
        &self.rectangle
    }
}
