CC=clang

SDL_BRIDGE_LIB:=libsdl_bridge.so
SDL_CFLAGS=$(shell pkg-config --cflags sdl2 SDL2_ttf)
SDL_RELEASE_FLAGS=${SDL_CFLAGS} -O2
SDL_LDFLAGS=$(shell pkg-config --libs sdl2 SDL2_ttf)
LIB_FLAGS:=-shared -fPIC
RELEASE_FILES=$(filter-out release/run.sh,$(wildcard release/*))

target/debug/pong: src/main.rs src/sdl/*.rs src/game_objects/*.rs ${SDL_BRIDGE_LIB}
	cargo build

${SDL_BRIDGE_LIB}: src/sdl/sdl_bridge.c
	$(CC) ${LIB_FLAGS} ${SDL_CFLAGS} -o $@ $< ${SDL_LDFLAGS}

.PHONY: release-all

release-all: release/pong release/res

release/pong: target/release/pong
	cp target/release/pong release/pong

release/res: res/**
	cp -r res release/res

target/release/pong: src/main.rs src/sdl/*.rs src/game_objects/*.rs release/${SDL_BRIDGE_LIB}
	cargo build --release

release/${SDL_BRIDGE_LIB}: src/sdl/sdl_bridge.c
	$(CC) ${LIB_FLAGS} -o $@ ${SDL_CFLAGS} $< ${SDL_LDFLAGS}

.PHONY: clean

clean:
	cargo clean
	rm -rf ${SDL_BRIDGE_LIB} pong ${RELEASE_FILES}
