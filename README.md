# Description
A simple clone of pong.

This is a Rust version of the C++ Pong clone I made earlier. This is my first Rust project. It doesn't have any crate dependencies. I didn't use existing SDL binding crates since I wanted to learn to call C libraries myself.

# Features
* Two independently controlled paddles
* A ball with some physics
* Score tracking

# Dependencies
* SDL2
* SDL2_ttf

# Building
Building is supported on unix-like environments. It uses a Makefile to build both a C bridge library and the Rust application. To create a debug build:

	make

You can also use the run.sh script to both create a debug build and run it in one command.

To create a release build, which is placed in a "release" directory with its own run.sh script:

	./build-release.sh

To build a mingw release for Windows, which is placed in a "release-mingw" directory:

	./build-mingw-release.sh

# Running
For the Linux release, run the "run.sh" script.

For the Windows release, run pong.exe.

# Controls

Player 1:

* w = move up
* s = move down

Player 2:

* arrow up = move up
* arrow down = move down
