#!/bin/sh
SCRIPT_DIR=$(dirname "$0")
cd "$SCRIPT_DIR"

while true
do
	find src/ -name "*.rs" -or -name "*.c" | entr -d ./locked-build.sh
done
