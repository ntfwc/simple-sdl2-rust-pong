#!/bin/sh
set -e
SCRIPT_DIR=$(dirname "$0")
cd "$SCRIPT_DIR"

./locked-build.sh
export LD_LIBRARY_PATH=".:$LD_LIBRARY_PATH"
target/debug/pong
